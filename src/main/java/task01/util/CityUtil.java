package main.java.task01.util;

import main.java.task01.model.City;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class CityUtil {

    public static void printCity(List<City> cities) {
        cities.forEach(System.out::println);
    }

    public static List<City> reader() {
        List<City> cities = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File("src/main/resources/city_ru.csv"));
            while (scanner.hasNextLine()) {
                cities.add(parseLine(scanner.nextLine()));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return cities;
    }

    private static City parseLine(String line) {
        String[] mass = line.split(";");
        String name = mass[1];
        String region = mass[2];
        String district = mass[3];
        String population = mass[4];
        String foundation = null;
        if (mass.length > 5) {
            foundation = mass[5];
        }
        return new City(name, region, district, population, foundation);
    }
}
