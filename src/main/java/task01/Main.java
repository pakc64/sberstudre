package main.java.task01;

import static main.java.task01.util.CityUtil.printCity;
import static main.java.task01.util.CityUtil.reader;

/**
 * Производит чтени файла и отображение списка субьектов в консоли
 */
public class Main {
    public static void main(String[] args) {
        printCity(reader());
    }
}
