package main.java.task02.model;

public class City {

    private String id;          //Порядковый номер записи справочника;
    private String name;        //Наименование города;
    private String region;      //Регион;
    private String district;    //Федеральный округ;
    private String population;  //Количество жителей;
    private String foundation;  //Дата основания или первое упоминание;

    public City(String name, String region, String district, String population, String foundation) {
        this.name = name;
        this.region = region;
        this.district = district;
        this.population = population;
        this.foundation = foundation;
    }

    public City(String name, String region, String district, String population) {
        this.name = name;
        this.region = region;
        this.district = district;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public String getDistrict() {
        return district;
    }

    @Override
    public String toString() {
        if (!(foundation == null)) {
            return "City{" +
                    "name='" + name + '\'' +
                    ", region='" + region + '\'' +
                    ", district='" + district + '\'' +
                    ", population='" + population + '\'' +
                    ", foundation='" + foundation + '\'' +
                    '}';
        } else {
            return "City{" +
                    "name='" + name + '\'' +
                    ", region='" + region + '\'' +
                    ", district='" + district + '\'' +
                    ", population='" + population + '\'' +
                    '}';
        }
    }
}