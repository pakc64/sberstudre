package main.java.task02;

import main.java.task02.model.City;

import java.util.Comparator;
import java.util.List;

import static main.java.task02.util.CityUtil.printCity;
import static main.java.task02.util.CityUtil.reader;

/**
 * Производит чтени файла и отображение списка субьектов в консоли
 * Производит сортировку списка по городам в алфавитном порядке
 * А так же по федеральным округам и городам
 */
public class Main {
    public static void main(String[] args) {
        List<City> cities = reader();
        sortedByNameVersion1(cities);

        sortedByNameVersion2(cities);

        sortedByDistrictAndName(cities);

        printCity(cities);
    }

    private static void sortedByNameVersion1(List<City> cities) {
        cities.sort(((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName())));
    }

    private static void sortedByNameVersion2(List<City> cities) {
        cities.sort(new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
    }

    private static void sortedByDistrictAndName(List<City> cities) {
        cities.sort(Comparator.comparing(City::getDistrict).thenComparing(City::getName));
    }

}
