package main.java.task04;

import main.java.task04.model.City;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static main.java.task04.util.CityUtil.reader;

/**
 *
 */
public class Main {
    public static void main(String[] args) {
        countCityInRegion(reader());

    }

    public static void countCityInRegion(List<City> cities) {
        Map<String, Integer> regionCityMap = new HashMap<>();
        for (City city : cities) {
            if (!regionCityMap.containsKey(city.getRegion())) {
                regionCityMap.put(city.getRegion(), 1);
            } else {
                int countCity = regionCityMap.get(city.getRegion()) + 1;
                regionCityMap.put(city.getRegion(), countCity);
            }
        }
        for (Map.Entry<String, Integer> pair : regionCityMap.entrySet()) {
            String region = pair.getKey();
            Integer numberOfCities = pair.getValue();
            System.out.println(region + " - " + numberOfCities);
        }
    }

}
