package main.java.task03.util;

import main.java.task03.model.City;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class CityUtil {

    public static List<City> reader() {
        List<City> cities = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File("src/main/resources/city_ru.csv"));
            while (scanner.hasNextLine()) {
                cities.add(parseLine(scanner.nextLine()));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return cities;
    }

    private static City parseLine(String line) {
        String[] mass = line.split(";");
        String name = mass[1];
        String region = mass[2];
        String district = mass[3];
        String population = mass[4];
        String foundation = null;
        if (mass.length > 5) {
            foundation = mass[5];
        }
        return new City(name, region, district, population, foundation);
    }

    public static void findMaxPopulation(List<City> cities) {
        int index = 0;
        int maxPopulation = 0;
        City[] citiesMass = cities.toArray(new City[0]);

        for (int i = 0; i < citiesMass.length; i++) {
            int populationCity = Integer.parseInt(citiesMass[i].getPopulation());
            if (populationCity > maxPopulation) {
                maxPopulation = populationCity;
                index = i;
            }
        }
        System.out.println("[" + index + "]" + " = " + maxPopulation);
    }

}
